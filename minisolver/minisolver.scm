;;; This module extends GNU Guix and is licensed under the same terms, those
;;; of the GNU GPL version 3 or (at your option) any later version.
;;;
;;; Copyright © 2023 Marek Felšöci

(define-module (minisolver minisolver)
  #:use-module (guix)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system cmake)
  #:use-module (gnu packages pkg-config)
  #:use-module (airbus solvers)
  #:use-module (guix utils))

(define-public minisolver
  (let ((commit "84ad1505eae887e371b3d9adb86618f9c4ef5014")
        (revision "2"))
    (package
     (name "minisolver")
     (version (git-version "0.1" revision commit))
     (home-page
      "https://gitlab.inria.fr/tutorial-guix-hpc-workshop/software/minisolver")
     (source
      (origin
       (method git-fetch)
       (uri
        (git-reference
         (url home-page)
         (commit commit)))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "0mjcgx9k5qynf60alg494w2zxc13xddncygq9g2f8j8ivzxj2v3b"))))
     (build-system cmake-build-system)
     (native-inputs (list pkg-config))
     (inputs (list hmat-oss))
     (synopsis
      "Testing dense solvers with pseudo-BEM matrices.")
     (description
      "A simple application for testing dense solvers with pseudo-BEM matrices.
It is based on the test_FEMBEM application, however with just enough
functionalities for the needs of the Org mode and Guix tutorial.")
     (license license:expat))))
